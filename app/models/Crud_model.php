<?php

class Crud_Model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
    }
    public function validate_user_credentials($username, $password){
    	return $this->db->get_where("users", array("email" => $username,"password" => hash ( "sha256",$password)))->row_array();
    }
    function get_image_url($id = '') {
         if (file_exists('assets/uploads/users/'. $id . '.jpg')){
            $image_url ='assets/uploads/users/'. $id . '.jpg';
        }else{
            $image_url ='assets/uploads/user.jpg';
        }
        return $image_url;
    }
    function insert_user_details($data){
        $this->db->insert('users',$data);
        $insert_id = $this->db->insert_id();
        
        return  $insert_id;
    }
    function get_user_details($id = '') {
  		return $this->db->get_where('users',array('id'=>$id))->row_array();
    }
    function get_role_details($id = '') {
  		return $this->db->get_where('roles',array('id'=>$id))->row_array();
    }
    function get_users_info(){
        return $this->db->order_by('id','DESC')->get_where('users',array('role_id'=>2,'row_status !='=> 0))->result_array();
    }
    function get_selected_users_info(){
        return $this->db->order_by('id','DESC')->get_where('users',array('role_id'=>2,'row_status'=> 1))->result_array();
    }
    public function delete_user($id){
        
       $this->db->where('id', $id);
       return $this->db->update('users',['row_status' => 0]);
    }
    public function change_status($id, $is_checked){
        $this->db->where('id', $id);
       if($is_checked == 'true'){
           return $this->db->update('users',['row_status' => 1]);
       }else{
           return $this->db->update('users',['row_status' => 2]);
       }
    }
    function update_system_settings() {
        if(!empty($this->input->post('system_name'))){
        $data['description'] = $this->input->post('system_name');
        $this->db->where('setting_type', 'system_name');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('system_title'))){
        $data['description'] = $this->input->post('system_title');
        $this->db->where('setting_type', 'system_title');
        $this->db->update('settings', $data);
        }   
        if(!empty($this->input->post('address'))){
        $data['description'] = $this->input->post('address');
        $this->db->where('setting_type', 'address');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('mobile'))){   
        $data['description'] = $this->input->post('mobile');
        $this->db->where('setting_type', 'mobile');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('system_email'))){
        $data['description'] = $this->input->post('system_email');
        $this->db->where('setting_type', 'system_email');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('email_password'))){
        $data['description'] = $this->input->post('email_password');
        $this->db->where('setting_type', 'email_password');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('fb'))){
        $data['description'] = $this->input->post('fb');
        $this->db->where('setting_type', 'facebook');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('twiter'))){
        $data['description'] = $this->input->post('twiter');
        $this->db->where('setting_type', 'twiter');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('youtube'))){
        $data['description'] = $this->input->post('youtube');
        $this->db->where('setting_type', 'youtube');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('sms_username'))){
        $data['description'] = $this->input->post('sms_username');
        $this->db->where('setting_type', 'sms_username');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('sms_sender'))){
        $data['description'] = $this->input->post('sms_sender');
        $this->db->where('setting_type', 'sms_sender');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('sms_hash'))){
        $data['description'] = $this->input->post('sms_hash');
        $this->db->where('setting_type', 'sms_hash');
        $this->db->update('settings', $data);
        }
        if(!empty($this->input->post('terms'))){
            $data['description'] = $this->input->post('terms');
            $this->db->where('setting_type', 'terms');
            $this->db->update('settings', $data);
        }
    }
    function update_user_password($password,$user_id){
        $password=hash ( "sha256",$password);
        $data['password'] = $password;
        $this->db->where('id', $user_id);
        return $this->db->update('users', $data);
    }
}

?>