<div class="row">
    <div class="col-md-4">
                    <section class="card card-featured-left card-featured-primary mb-4">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-primary">
                                                    <i class="fas fa-user"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Today Registered Users</h4>
                                                    <div class="info">
                                                        <strong class="amount"><?=$this->db->get_where('users',array('created_at >='=>date('Y-m-d 00:00:00'),'created_at <='=>date('Y-m-d 23:59:59')))->num_rows();?></strong>
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <!-- <a class="text-muted text-uppercase">(view all)</a> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                             <div class="col-md-4">
                    <section class="card card-featured-left card-featured-primary mb-4">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-primary">
                                                    <i class="fas fa-users"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Total Users</h4>
                                                    <div class="info">
                                                        <strong class="amount"><?=$this->db->get_where('users',array('row_status !='=>0,'role_id'=>2))->num_rows();?></strong>
                                                        <!-- <span class="text-primary">(14 unread)</span> -->
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <a href="<?=base_url('users');?>" class="text-muted text-uppercase">(view all)</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                             <div class="col-md-4">
                    <section class="card card-featured-left card-featured-primary mb-4">
                                    <div class="card-body">
                                        <div class="widget-summary">
                                            <div class="widget-summary-col widget-summary-col-icon">
                                                <div class="summary-icon bg-primary">
                                                    <i class="fas fa-users"></i>
                                                </div>
                                            </div>
                                            <div class="widget-summary-col">
                                                <div class="summary">
                                                    <h4 class="title">Selected Users</h4>
                                                    <div class="info">
                                                        <strong class="amount"><?=$this->db->get_where('users',array('row_status ='=>1,'role_id'=>2))->num_rows();?></strong>
                                                        <span class="text-primary"></span>
                                                    </div>
                                                </div>
                                                <div class="summary-footer">
                                                    <a href="<?=base_url('selected_user');?>" class="text-muted text-uppercase">(view all)</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                
                            </div>
                            </div>
                            