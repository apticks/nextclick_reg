
<html>
	<head>
		<title>Porto Admin - Invoice Print</title>
		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.css" />

		<!-- Invoice Print Style -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/invoice-print.css" />
	</head>
	<body>
		<div class="invoice">
		<?php
$user=$this->crud_model->get_user_details($user_id);
?>
<section class="card">
						<div class="card-body">
							<div class="invoice">
								<header class="clearfix">
									<div class="row">
										<div class="col-sm-6 mt-3">
											<h2 class="h2 mt-0 mb-1 text-dark font-weight-bold"><?=ucwords($user['name']);?></h2>
											<p class="h5 mb-1 text-dark font-weight-semibold">Applying for: <?=$user['post_applying']?></p>
										</div>
										<div class="col-sm-6 text-right mt-3 mb-3">
											<address class="ib mr-5">Current Address:<br/>
												<?=$user['current_address']?><br/>
											</address>
										</div>
									</div>
								</header>
								
							<p class="h5 mb-1 text-dark font-weight-semibold">Educational Details:</p>
								<table class="table table-responsive-md invoice-items">
									
										
										<tr class="text-dark">
											<th class="text-dark">Mobile:</th>
											<th class="text-dark"><?=$user['mobile'].', '.$user['mobile2'];?></th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Whats App:</th>
											<th class="text-dark"><?=$user['whatsapp_number'];?></th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Peremanent Address:</th>
											<th class="text-dark">
												<?=$user['permanent_address']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Educational Qualifications:</th>
											<th class="text-dark">
												<?=$user['e_qualifications']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Job Experience:</th>
											<th class="text-dark">
												<?=$user['job_experience']?>
											</th>
										</tr>
										<?php
										if($user['job_experience']=='Yes'){
										?>
										<tr class="text-dark">
											<th class="text-dark">Worked Before:</th>
											<th class="text-dark">
												<?=$user['worked_before']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Work Experience:</th>
											<th class="text-dark">
												<?=$user['years_worked']?>
											</th>
										</tr>
										<?php }?>
										<tr class="text-dark">
											<th class="text-dark">Postal:</th>
											<th class="text-dark">
												<?=$user['postal']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">State:</th>
											<th class="text-dark">
												<?=$user['state']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">District:</th>
											<th class="text-dark">
												<?=$user['district']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Constituency:</th>
											<th class="text-dark">
												<?=$user['constituency']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Father Name:</th>
											<th class="text-dark"><?=$user['father_name'];?></th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Date Of Birth:</th>
											<th class="text-dark">
												<?=$user['dob']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Gender:</th>
											<th class="text-dark">
												<?=$user['gender']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Marital Status:</th>
											<th class="text-dark">
												<?=$user['marital_status']?>
											</th>
										</tr>
										<tr class="text-dark">
											<th class="text-dark">Languages:</th>
											<th class="text-dark">
												<?=$user['languages']?>
											</th>
										</tr>
										
										
										
								</table>
							</div>
						</div>
					</section>
		</div>

		<script>
			window.print();
		</script>
	</body>
</html>