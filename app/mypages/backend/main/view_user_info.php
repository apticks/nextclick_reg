<?php
$user = $this->crud_model->get_user_details($user_id);
?>
<style>
img{
    height:170px
    width:auto;/*maintain aspect ratio*/
    max-width:500px;
}
</style>
<section class="card">

	<div class="card-body" id='DivIdToPrint'>
		<div class="invoice">
			<header class="clearfix">
				<div class="row">
					<div class="col-sm-6 mt-3">
						<h2 class="h2 mt-0 mb-1 text-dark font-weight-bold"><?=ucwords($user['name']);?></h2>
						<p class="h5 mb-1 text-dark font-weight-semibold">Applying for: <?=$user['post_applying']?></p>
					</div>
					<div class="col-sm-6 text-right mt-3 mb-3">
						<div class="row">
						<div class="col-md-6">
						<a class="img-thumbnail lightbox"
								href="<?=base_url();?>uploads/profiles/<?=$user['id'];?>.jpg"
								data-plugin-options="{ &quot;type&quot;:&quot;image&quot; }"> <img
								class="img-fluid"
								src="<?=base_url();?>uploads/profiles/<?=$user['id'];?>.jpg" style="height: 150px;"> <span
								class="zoom"> <i class="fas fa-search"></i>
							</span>
							</a>
							<div class="col-md-12">
						<form  action="<?php echo base_url();?>main/update_profile" enctype='multipart/form-data' method="post">
							<input type="file" class="btn btn-secondary btn-sm" name="profile" style="width: 108px;" />
							<input type="hidden" name="id" value="<?php echo $this->uri->segment('2');?>" />
							<button class="btn btn-success btn-sm">update</button>
						</form>
					</div>
						</div>
						<div class="col-md-6">
						<address class="ib mr-5">
							Current Address:<br />
												<?=$user['current_address']?><br />
						</address>
						</div>
					</div>
						
					</div>
				</div>
			</header>


			<p class="h5 mb-1 text-dark font-weight-semibold">User Details:    <br>
			<div style="margin-left:800px;margin-top:-42px;">
				<?php if($user['row_status'] == 1){?>
					<button type="button" class="btn btn-outline-primary  enableOnInput " data-toggle="modal" data-target="#myModal" id="a" >Create Employee</button>
				<?php }else{?>
					<input type="checkbox"  exe_id="<?php echo $user['id']; ?>" class="approve_toggle" data-toggle="toggle"  data-style="ios" data-on="Select" data-off="Deselect" data-onstyle="success" data-offstyle="danger"></p>
				<?php }?>
			</div>                                     

			<table class="table table-responsive-md invoice-items">


				<tr class="text-dark">
					<th class="text-dark">Mobile:</th>
					<th class="text-dark"><?=$user['mobile'].', '.$user['mobile2'];?></th>
					
							
                                                    
				</tr>
				<tr class="text-dark">
					<th class="text-dark">EMP Id:</th>
					<th class="text-dark"><?=$user['unique_id'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Whats App:</th>
					<th class="text-dark"><?=$user['whatsapp_number'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Permanent Address:</th>
					<th class="text-dark"><?=$user['permanent_address']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Educational Qualifications:</th>
					<th class="text-dark"><?=$user['e_qualifications']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Job Experience:</th>
					<th class="text-dark"><?=$user['job_experience']?></th>
				</tr>
										<?php
        if ($user['job_experience'] != 'No Experience') {
            ?>
				<tr class="text-dark">
					<th class="text-dark">Worked Before:</th>
					<th class="text-dark"><?=$user['worked_before']?></th>
				</tr>
				<!-- <tr class="text-dark">
											<th class="text-dark">Work Experience:</th>
											<th class="text-dark">
												<?=$user['years_worked']?>
											</th>
										</tr> -->
										<?php }?>
				<tr class="text-dark">
					<th class="text-dark">Postal:</th>
					<th class="text-dark"><?=$user['postal']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">State:</th>
					<th class="text-dark"><?=$user['state']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">District:</th>
					<th class="text-dark"><?=$user['district']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Constituency:</th>
					<th class="text-dark"><?=$user['constituency']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Father Name:</th>
					<th class="text-dark"><?=$user['father_name'];?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Date Of Birth:</th>
					<th class="text-dark"><?=$user['dob']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Gender:</th>
					<th class="text-dark"><?=$user['gender']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Marital Status:</th>
					<th class="text-dark"><?=$user['marital_status']?></th>
				</tr>
				<tr class="text-dark">
					<th class="text-dark">Languages:</th>
					<th class="text-dark"><?=$user['languages']?></th>
				</tr>
				<tr>
					<th class="text-dark">Id Proof</th>
					<th class="text-dark">SSC</th>
					<th class="text-dark">Inter</th>
					<th class="text-dark">Degree</th>
				</tr>
				<tr>
					<th class="text-dark"><div class="thumbnail-gallery">
							<a class="img-thumbnail lightbox"
								href="<?=base_url();?>uploads/id_proofs/<?=$user['id'];?>.jpg"
								data-plugin-options="{ &quot;type&quot;:&quot;image&quot; }"> <img
								class="img-fluid" style="width: 215px !important;height: 215px !important;"
								src="<?=base_url();?>uploads/id_proofs/<?=$user['id'];?>.jpg"> <span
								class="zoom"> <i class="fas fa-search"></i>
							</span>
							</a>
						<form  action="<?php echo base_url();?>main/update_id_proof" enctype='multipart/form-data' method="post">
							<input type="file" class=""  name="id_proof"/>
							<input type="hidden" name="id" value="<?php echo $this->uri->segment('2');?>" /><br/><br/>
							<button class="btn btn-success">update</button>
						</form>
						</div></th>
							<th class="text-dark"><div class="thumbnail-gallery">
							<a class="img-thumbnail lightbox"
								href="<?=base_url();?>uploads/ssc/<?=$user['id'];?>.jpg"
								data-plugin-options="{ &quot;type&quot;:&quot;image&quot; }"> <img
								class="img-fluid" style="width: 215px !important;height: 215px !important;"
								src="<?=base_url();?>uploads/ssc/<?=$user['id'];?>.jpg"> <span
								class="zoom"> <i class="fas fa-search"></i>
							</span>
							</a>
							<form  action="<?php echo base_url();?>main/update_ssc" enctype='multipart/form-data' method="post">
							<input type="file" class="" name="ssc"/>
							<input type="hidden" name="id" value="<?php echo $this->uri->segment('2');?>" /><br/><br/>
							<button class="btn btn-success">update</button>
						</form>
						</div></th>
							<th class="text-dark"><div class="thumbnail-gallery">
							<a class="img-thumbnail lightbox"
								href="<?=base_url();?>uploads/inter/<?=$user['id'];?>.jpg"
								data-plugin-options="{ &quot;type&quot;:&quot;image&quot; }"> <img
								class="img-fluid" style="width: 215px !important;height: 215px !important;"
								src="<?=base_url();?>uploads/inter/<?=$user['id'];?>.jpg"> <span
								class="zoom"> <i class="fas fa-search"></i>
							</span>
							</a>
							<form  action="<?php echo base_url();?>main/update_inter" enctype='multipart/form-data' method="post">
							<input type="file" class="" name="inter"/>
							<input type="hidden" name="id" value="<?php echo $this->uri->segment('2');?>" /><br/><br/>
							<button class="btn btn-success">update</button>
						</form>
						</div></th>
					<th class="text-dark"><div class="thumbnail-gallery">
							<a class="img-thumbnail lightbox"
								href="<?=base_url();?>uploads/degree/<?=$user['id'];?>.jpg"
								data-plugin-options="{ &quot;type&quot;:&quot;image&quot; }"> <img
								class="img-fluid" style="width: 215px !important;height: 215px !important;"
								src="<?=base_url();?>uploads/degree/<?=$user['id'];?>.jpg"> <span
								class="zoom"> <i class="fas fa-search"></i>
							</span>
							</a>
							<form  action="<?php echo base_url();?>main/update_degree" enctype='multipart/form-data' method="post">
							<input type="file" class="" name="degree"/>
							<input type="hidden" name="id" value="<?php echo $this->uri->segment('2');?>" /><br/><br/>
							<button class="btn btn-success">update</button>
						</form>
						</div></th>
				</tr>


			</table>
			
					</div>
		</div>
		
	</div>
	<div class="card-body">
		<div class="text-right mr-4">
			<a href="#" onclick='printDiv("DivIdToPrint");' target="_blank"
				class="btn btn-primary ml-3"><i class="fas fa-print"></i> Print</a>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<h2>Select Role</h2>
					<p></p>
						<div class="radio" id="roles">
							
						</div>
						<input type="hidden" name="email" id="email" value="<?=$user['email'];?>"/>
						<input type="hidden" name="name" id="name" value="<?=$user['name'];?>"/>
						<input type="hidden" name="mobile" id="mobile" value="<?=$user['mobile']?>"/>
						<input type="hidden" name="id" id="id" value="<?=$user['id'];?>"/>
				</div>
				<div class="modal-footer">
					<button type="button"  class="btn btn-success" id="create-emp">Create User</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
</section>
<script type="text/javascript">
   function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
