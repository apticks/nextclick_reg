<style>
 table.table {
    clear: both;
    margin-bottom: 6px !important;
    max-width: none !important;
    table-layout: fixed;
    word-break: break-all;
   } 
   
   
</style>

                          <div class="col">
                                <section class="card">
                                    <div class="card-body" >
                                     <div class="row">
                                    <table class="table table-bordered table-striped mb-0" id="main-users" >
                                            <thead>
                                                <tr>
                                                    <th>Sno</th>
                                                    <th>Name</th>
                                                    <th>Email ID</th>
                                                    <th>PhoneNo</th>
                                                    <th>Post Applying</th>
                                                    <th>District</th>
                                                    <th>Constituency</th>
                                                    <th>Date</th>
                                                    <th >Actions</th>
                                                    <th >Select</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i=1;
                                                foreach ($mainuser_data as $row) {
                                                ?>
                                                <tr>
                                                    <td ><?=$i?></td>
                                                    <td><?=$row['name'];?></td>
                                                    <td><?=$row['email'];?></td>
                                                    <td><?=$row['mobile'];?></td>
                                                  <td><?=$row['post_applying'];?></td>
                                                    <td><?=$row['district'];?></td>
                                                    <td><?=$row['constituency'];?></td>
                                                    <td><?= $row['created_at']?></td>
                                                    <td>
                                                       <!--  <a href="#" class=" mr-2  text-primary">
                                                            <i class="fas fa-pencil-alt"></i>
                                                        </a> -->
                                                        <a href="<?=base_url('view_user_info/').$row['id'];?>" class="mr-2  text-success">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                        <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $row['id']?>,'user_delete' )">
											<i class="far fa-trash-alt"></i></a>
                                                    </td>
                                           			<td><input id="toggle<?php echo $row['id'];?>" exe_id="<?php echo $row['id'];?>"  type="checkbox" class="approve_toggle"    data-toggle="toggle" data-style="ios" data-on="Select" data-off="Deselect" data-onstyle="success" data-offstyle="danger"></td>
                                       
                                           
                                                </tr>
                                            <?php $i++;}?>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                        </div>
<script type="text/javascript">
$(document).ready(function(){
	<?php foreach ($mainuser_data as $row) { ?>
		<?php if($row['row_status'] == '1'){?>
			$('#toggle<?php echo $row['id']?>').bootstrapToggle('on')
		<?php }else{ ?>
			$('#toggle<?php echo $row['id']?>').bootstrapToggle('off')
		<?php }?>
	<?php }?>
});
</script>