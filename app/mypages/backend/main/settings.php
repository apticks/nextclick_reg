<!-- 
<div class="ui-pnotify notification-success" style="width: 300px; right: 15px; top: 15px; opacity: 0.282384; cursor: auto;"><div class="notification ui-pnotify-container notification-warning" style="min-height: 16px;"><div class="ui-pnotify-closer" style="cursor: pointer; visibility: hidden;"><span class="fas fa-times" title="Close"></span></div><div class="ui-pnotify-sticker" style="cursor: pointer; visibility: hidden;"><span class="fas fa-pause" title="Stick"></span></div><div class="ui-pnotify-icon"><span class="fas fa-check"></span></div><h4 class="ui-pnotify-title">Congratulations</h4><div class="ui-pnotify-text">You completed the wizard form.</div><div style="margin-top: 5px; clear: both; text-align: right; display: none;"></div></div>
</div> -->
<!-- <div class="row">
                            <div class="col-lg-6">
                                <section class="card form-wizard" id="w1">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                                        </div>
                        
                                        <h2 class="card-title">Form Wizard</h2>
                                    </header>
                                    <div class="card-body card-body-nopadding">
                                        <div class="wizard-tabs">
                                            <ul class="nav wizard-steps">
                                                <li class="nav-item active">
                                                    <a href="#w1-account" data-toggle="tab" class="nav-link text-center">
                                                        <span class="badge">1</span>
                                                        Account
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#w1-profile" data-toggle="tab" class="nav-link text-center">
                                                        <span class="badge">2</span>
                                                        Profile
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#w1-confirm" data-toggle="tab" class="nav-link text-center">
                                                        <span class="badge">3</span>
                                                        Confirm
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <form class="form-horizontal" novalidate="novalidate">
                                            <div class="tab-content">
                                                <div id="w1-account" class="tab-pane p-3 active">
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w1-username">Username</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="username" id="w1-username" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w1-password">Password</label>
                                                        <div class="col-sm-8">
                                                            <input type="password" class="form-control" name="password" id="w1-password" minlength="6" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="w1-profile" class="tab-pane p-3">
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w1-first-name">First Name</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="first-name" id="w1-first-name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w1-last-name">Last Name</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="last-name" id="w1-last-name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="w1-confirm" class="tab-pane p-3">
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w1-email">Email</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="email" id="w1-email" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-sm-10">
                                                            <div class="checkbox-custom">
                                                                <input type="checkbox" name="terms" id="w1-terms" required>
                                                                <label for="w1-terms">I agree to the terms of service</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <ul class="pager">
                                            <li class="previous disabled">
                                                <a><i class="fas fa-angle-left"></i> Previous</a>
                                            </li>
                                            <li class="finish hidden float-right">
                                                <a>Finish</a>
                                            </li>
                                            <li class="next">
                                                <a>Next <i class="fas fa-angle-right"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </section>
                            </div>
                            <div class="col-lg-6">
                                <section class="card form-wizard" id="w2">
                                    <div class="tabs">
                                        <ul class="nav nav-tabs nav-justify">
                                            <li class="nav-item active">
                                                <a href="#w2-account" data-toggle="tab" class="nav-link text-center">
                                                    <span class="badge badge-primary">1</span>
                                                    Account
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#w2-profile" data-toggle="tab" class="nav-link text-center">
                                                    <span class="badge badge-primary">2</span>
                                                    Profile
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#w2-confirm" data-toggle="tab" class="nav-link text-center">
                                                    <span class="badge badge-primary">3</span>
                                                    Confirm
                                                </a>
                                            </li>
                                        </ul>
                                        <form class="form-horizontal" novalidate="novalidate">
                                            <div class="tab-content">
                                                <div id="w2-account" class="tab-pane p-3 active">
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w2-username">Username</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" id="w2-username" name="username" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w2-password">Password</label>
                                                        <div class="col-sm-8">
                                                            <input type="password" class="form-control" name="password" id="w2-password" required minlength="6">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="w2-profile" class="tab-pane p-3">
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w2-first-name">First Name</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="first-name" id="w2-first-name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w2-last-name">Last Name</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="last-name" id="w2-last-name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="w2-confirm" class="tab-pane p-3">
                                                    <div class="form-group row">
                                                        <label class="col-sm-4 control-label text-sm-right pt-1" for="w2-email">Email</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" class="form-control" name="email" id="w2-email" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-sm-10">
                                                            <div class="checkbox-custom">
                                                                <input type="checkbox" name="terms" id="w2-terms" required>
                                                                <label for="w2-terms">I agree to the terms of service</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <ul class="pager">
                                            <li class="previous disabled">
                                                <a><i class="fas fa-angle-left"></i> Previous</a>
                                            </li>
                                            <li class="finish hidden float-right">
                                                <a>Finish</a>
                                            </li>
                                            <li class="next">
                                                <a>Next <i class="fas fa-angle-right"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </section>
                            </div>
                        </div> -->

                    <div class="row">
                        <div class="col-md-6">
                            <form id="form" action="<?=base_url('main/system_settings')?>" class="form-horizontal" novalidate="novalidate" method="post"> 
                                <section class="card">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                        </div>
                                        <h2 class="card-title">System Settings</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">System Name <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="system_name" class="form-control" placeholder="System Name" required="" value="<?=$this->db->get_where('settings', array('setting_type' => 'system_name'))->row()->description; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">System Title <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="system_title" class="form-control" placeholder="System Title" required="" value="<?=$this->db->get_where('settings', array('setting_type' => 'system_title'))->row()->description; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">System Email <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <span class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="fas fa-envelope"></i>
                                                        </span>
                                                    </span>
                                                    <input type="email" name="system_email" class="form-control" placeholder="eg.: email@email.com" required="" value="<?=$this->db->get_where('settings', array('setting_type' => 'system_email'))->row()->description; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Email Password <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <span class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="fas fa-key"></i>
                                                        </span>
                                                    </span>
                                                    <input type="password" name="email_password" class="form-control" placeholder="eg.: ******" required="" value="<?=$this->db->get_where('settings', array('setting_type' => 'email_password'))->row()->description; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Mobile Number<span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" required="" value="<?=$this->db->get_where('settings', array('setting_type' => 'mobile'))->row()->description; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Address <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <textarea name="address" rows="5" class="form-control" placeholder="Enter Address" required=""><?=$this->db->get_where('settings', array('setting_type' => 'address'))->row()->description; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                <label class="col-sm-3 control-label text-sm-right pt-2">Facebook Link</label>
                                            <div class="col-sm-9">
                                                <input type="url" name="fb" class="form-control" placeholder="eg.: https://facebook.com" value="<?=$this->db->get_where('settings', array('setting_type' => 'facebook'))->row()->description; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Twiter Link</label>
                                            <div class="col-sm-9">
                                                <input type="url" name="twiter" class="form-control" placeholder="eg.: https://twiter.com" value="<?=$this->db->get_where('settings', array('setting_type' => 'twiter'))->row()->description; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Youtube Link</label>
                                            <div class="col-sm-9">
                                                <input type="url" name="youtube" class="form-control" placeholder="eg.: https://youtube.com" value="<?=$this->db->get_where('settings', array('setting_type' => 'youtube'))->row()->description; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row justify-content-end">
                                            <div class="col-sm-9">
                                                <button class="btn btn-primary">Submit</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </footer>
                                </section>
                            </form>
                        </div>
                         <div class="col-md-6">
                            <form id="form" action="<?=base_url('main/system_settings')?>" class="form-horizontal" novalidate="novalidate"  method="post">
                                <section class="card">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                        </div>

                                        <h2 class="card-title">SMS Settings</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Username <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="sms_username" class="form-control" placeholder="System Name" required="" value="<?=$this->db->get_where('settings', array('setting_type' => 'sms_username'))->row()->description; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Sender <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="sms_sender" class="form-control" placeholder="System Title" required="" value="<?=$this->db->get_where('settings', array('setting_type' => 'sms_sender'))->row()->description; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 control-label text-sm-right pt-2">Hash Key <span class="required">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="sms_hash" class="form-control" placeholder="System Title" required="" value="<?=$this->db->get_where('settings', array('setting_type' => 'sms_hash'))->row()->description; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                          <div class="col col-sm col-md" ><label>Terms And Conditions</label>
                          				<textarea cols="80" id="add_terms" name="terms" rows="10" data-sample-short><?=$this->db->get_where('settings', array('setting_type' => 'terms'))->row()->description; ?></textarea>
                          				<?php echo form_error('terms', '<div style="color:red">', '</div>');?>
                        				</div>
                                        
                                        </div>
                                         <footer class="card-footer"> 
                                        <div class="row justify-content-end">
                                            <div class="col-sm-9">
                                                <button class="btn btn-primary">Submit</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </div>
                                        </div>
                                    </footer>
                                         </form>
                                        </section>
                                       
                        </div>
                    </div>
                    
                    
                    
                                        
                                     
                   
                   