<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
<style>
 table.table {
    clear: both;
    margin-bottom: 6px !important;
    max-width: none !important;
    table-layout: fixed;
    word-break: break-all;
   } 
</style>
                   <div class="row">
                            <div class="col">
                                <section class="card">
                                    
                                    <div class="card-body">
                                        <table class="table table-bordered table-striped mb-0" id="main-users">
                                            <thead>
                                                <tr>
                                                    <th >Sno</th>
                                                    <th>Emp id</th>
                                                    <th>Name</th>
                                                    <th>Email ID</th>
                                                    <th>PhoneNo</th>
                                                    <th>District</th>
                                                    <th>Post Applying</th>
                                                    <th>Date</th>
                                                   <th >Actions</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i=1;
                                                foreach ($selecteduser_data as $row) {
                                                ?>
                                                <tr>
                                                    <td><?=$i?></td>
                                                     <td><?=$row['unique_id'];?></td>
                                                    <td><?=$row['name'];?></td>
                                                    <td><?=$row['email'];?></td>
                                                    <td><?=$row['mobile'];?></td>
                                                    <td><?=$row['district'];?></td>
                                                    <td><?=$row['post_applying'];?></td>
                                                    <td><?= $row['created_at']?></td>
                                                   <td>
                                                       <!--  <a href="#" class=" mr-2  text-primary">
                                                            <i class="fas fa-pencil-alt"></i>
                                                        </a> -->
                                                        <a href="<?=base_url('view_user_info/').$row['id'];?>" class="mr-2  text-success">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                        <a href="#" class="mr-2  text-danger " onClick="delete_record(<?php echo $row['id']?>,'user_delete' )">
											<i class="far fa-trash-alt"></i></a>
                                                    </td>
                                                   
                                                </tr>
                                            <?php $i++;}?>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                        </div>
 
                        