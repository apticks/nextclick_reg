        <!-- Vendor CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/animate/animate.css">

        <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/all.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

        <!-- Specific Page Vendor CSS -->       
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/jquery-ui/jquery-ui.css" />     
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/jquery-ui/jquery-ui.theme.css" />       
<!--    <link rel="stylesheet" href="vendor/bootstrap-multiselect/css/bootstrap-multiselect.css" />     
    <link rel="stylesheet" href="vendor/morris/morris.css" />-->
    
    <link href="<?php echo base_url();?>assets/vendor/pnotify/pnotify.custom.css" rel="stylesheet" />
    
   <!-- Specific css for this page --> 
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.css" />

        <!-- Theme CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme.css" />

        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">
<!-- boostrap toggle button -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap4-toggle.min.css">
        <!-- Head Libs -->
    <script src="<?php echo base_url();?>assets/vendor/modernizr/modernizr.js"></script>       
    <script src="<?php echo base_url();?>assets/master/style-switcher/style.switcher.localstorage.js"></script>
     <style>
  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 10rem; }
  .toggle.ios .toggle-handle { border-radius: 10rem; }
</style>