<!doctype html>
<html class="fixed">
	
<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="Admin- CA Exam Series" />
		<meta name="description" content="Admin- CA Exam Series">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/animate/animate.css">

		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/all.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>assets/vendor/modernizr/modernizr.js"></script>		
        <script src="<?php echo base_url();?>assets/master/style-switcher/style.switcher.localstorage.js"></script>

	</head>
<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="" class="logo float-left">
					<img src="<?php echo base_url();?>assets/uploads/logo.png" height="80" alt="Admin- CA Exam Series" />
				</a>

				<div class="panel card-sign">
					<div class="card-title-sign mt-3 text-right">
						<h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Admin</h2>
       <?php if(!empty($_SESSION['error_msg'])){ ?>
		<div class="alert alert-danger"><?php echo $_SESSION['error_msg'];?></div>
		<?php }?>
					</div>
					<div class="card-body">
							<form action="<?=base_url();?>login_action" method="post">
							<div class="form-group mb-3">
								<label>Username</label>
								<div class="input-group">
									<input name="email" type="text" class="form-control form-control-lg" />
									<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-3">
								<div class="clearfix">
									<label class="float-left">Password</label>
									<a href="" class="float-right">Lost Password?</a>
								</div>
								<div class="input-group">
									<input name="password" type="password" class="form-control form-control-lg" />
									<span class="input-group-append">
										<span class="input-group-text">
											<i class="fas fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="RememberMe" name="rememberme" type="checkbox"/>
										<label for="RememberMe">Remember Me</label>
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary mt-2">Sign In</button>
								</div>
							</div>

							

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-3 mb-3">&copy; Copyright 2019. All Rights Reserved.</p>
			</div>
		</section>

       <!-- Vendor -->
<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.js"></script>		
<script src="<?php echo base_url();?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>		<script src="<?php echo base_url();?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url();?>assets/master/style-switcher/style.switcher.js"></script>				
<script src="<?php echo base_url();?>assets/vendor/popper/umd/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.js"></script>		
<script src="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>		
<script src="<?php echo base_url();?>assets/vendor/common/common.js"></script>		
<script src="<?php echo base_url();?>assets/vendor/nanoscroller/nanoscroller.js"></script>		
<script src="<?php echo base_url();?>assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>		
<script src="<?php echo base_url();?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url();?>assets/js/theme.js"></script>
		
		<!-- Theme Custom -->
<script src="<?php echo base_url();?>assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
<script src="<?php echo base_url();?>assets/js/theme.init.js"></script>
		<!-- Analytics to Track Preview Website -->		
	</body>

</html>