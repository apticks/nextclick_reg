
<!-- Vendor -->
<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/jquery-cookie/jquery.cookie.js"></script>
<!-- <script src="master/style-switcher/style.switcher.js"></script> -->
<script
	src="<?php echo base_url();?>assets/vendor/popper/umd/popper.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/vendor/common/common.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/nanoscroller/nanoscroller.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Specific Page Vendor -->
<script
	src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery-ui.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/jqueryui-touch-punch/jquery.ui.touch-punch.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/jquery-appear/jquery.appear.js"></script>



<!-- Specific css for this page -->
<script
	src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>

<script
	src="<?php echo base_url();?>assets/vendor/ios7-switch/ios7-switch.js"></script>



<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url();?>assets/js/theme.js"></script>

<!-- Theme Custom -->
<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="<?php echo base_url();?>assets/js/theme.init.js"></script>


<!-- Examples -->
<script
	src="<?php echo base_url();?>assets/js/examples/examples.datatables.tabletools.js"></script>
<script
	src="<?php echo base_url();?>assets/vendor/pnotify/pnotify.custom.js"></script>

<!-- Validation -->
<script
	src="<?php echo base_url();?>assets/vendor/jquery-validation/jquery.validate.js"></script>
<script
	src="<?php echo base_url();?>assets/js/examples/examples.validation.js"></script>
<!-- bootstrap toogle button -->
<script src="<?php echo base_url();?>assets/js/bootstrap4-toggle.min.js"></script>
<!-- Ckeditor library -->
<script src="https://cdn.ckeditor.com/4.13.0/standard-all/ckeditor.js"></script>
<script src="<?php echo base_url();?>assets/js/init-ckeditor.js"></script>


<script type="text/javascript">
    function get_plan_options(course_id){
$.ajax({
            url: '<?php echo base_url();?>ajax/get_plan_options_by_id/' + course_id ,
            success: function(response)
            {
                jQuery('#plans_list').html(response);
                get_subject_options(course_id);
            }
    });
    }
    function get_subject_options(course_id){
    $.ajax({
            url: '<?php echo base_url();?>ajax/get_subjects_by_id/' + course_id ,
            success: function(response)
            {
                jQuery('#subjects_list').html(response);
            }
    });    
    }
</script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<link
	href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script>
function delete_record(id){
	if(confirm('Do you want to delete..?')){
		$.ajax({
			url: 'main/user_delete',
			type: 'post',
			data: {id : id},
			success: function(data){
				window.location.reload();
			}
		});
	}
}

$(document).ready(function(){
	deleteAllCookies();
	/*Toggle*/
	$('input[type="checkbox"]').one('change', function(){
         if(confirm('Do You Want To select..?')){
            var id = $(this).attr('exe_id');
            var  is_checked = $(this).is(':checked');
            $.ajax({
                 url : base_url+'main/change_status', 
                 method : 'post',
                 dataType : 'json',
                 data :{id: id, is_checked: is_checked},
                 success : function(data){
                    console.log(data);
                    window.location.reload();
                 }
             });
         }
	});

    /*Create Employee*/
 	var options = '';
     $.ajax({
         url : parent_url+'auth/api/auth/roles',
         method : 'get',
         dataType : 'json',
         success : function(data){
            console.log(data.data);
        	$.each(data.data, function(index, element) {
        		options += '<div class="radio"><label><input type="radio" name="group_id" class="group_id" value="'+element.id+'" >'+element.name+'</label></div>';
        	});
           $('#roles').append(options);
         }
     });
     
	var unique_id = '';
     $('#create-emp').click(function(){
    	
        var id = $("#id").val();
		var name = $("#name").val();
		var email = $("#email").val();
		var image = readTextFile(base_url+"uploads/profile/"+id+".jpg");
		var mobile = $("#mobile").val();
		var group_id = $('input[name=group_id]:checked').val();
		 $.ajax({
	         url : parent_url+'auth/api/auth/create_user/'+group_id,
	         method : 'post',
	         dataType : 'json',
	         data:{name: name,email: email, group_id: group_id, mobile: mobile},
	         success : function(data){
	            if(data.http_code == 200){
	            	unique_id = data.data.unique_id;
	            	alert(data.message);
	            	$.ajax({
	                    url : base_url+'update_userid',
	                    method : 'post',
	                    dataType : 'json',
	                    data:{id: id,unique_id: unique_id},
	                    success : function(data){
	                       console.log(data);
	                       window.location.reload();
	                    }
	                });
	            }else{
					alert(data.message);
					window.location.reload();
	            }
	         }
	     }); 
     });
});
function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var allText = rawFile.responseText;
                console.log(allText);
                var image = new XMLSerializer().serializeToString(rawFile);
                var base64 = w.btoa( image );
                /* const toBase64 = rawFile => new Promise((resolve, reject) => {
            	    const reader = new FileReader();
            	    reader.readAsDataURL(rawFile);
            	    reader.onload = () => resolve(reader.result);
            	    reader.onerror = error => reject(error);
            	}); */
				console.log(base64);
            	return base64;
            }
        }
    }
    rawFile.send(null);
}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    return true;
}
  </script>
