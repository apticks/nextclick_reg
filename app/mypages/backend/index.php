<?php
$login_image_url = $this->crud_model->get_image_url($this->session->userdata('login_id'));
$login_user_details = $this->crud_model->get_user_details($this->session->userdata('login_id'));
$login_role_details = $this->crud_model->get_role_details($this->session->userdata('role_id'));
$system_name = $this->db->get_where('settings', array(
    'setting_type' => 'system_name'
))->row()->description;
$system_title = $this->db->get_where('settings', array(
    'setting_type' => 'system_title'
))->row()->description;
?>
<!doctype html>
<html class="fixed sidebar-left-sm"
	data-style-switcher-options="{'sidebarSize': 'sm'}">
<head>

<!-- Basic -->
<meta charset="UTF-8">

<title><?=ucwords($page_title);?>-<?=$system_name;?></title>
<meta name="description" content="Next Click">
<meta name="author" content="">

<!-- Mobile Metas -->
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<script src="env.js?ver=1.1"></script>  
<script type="text/javascript" src="simpleLoader.js?ver=1.1"></script>  
<script type="text/javascript" src="init.js?ver=1.1"></script> 
<!-- Web Fonts  -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light"
	rel="stylesheet" type="text/css">

<?php
include_once ('topcss.php');
?>
 <style>
input[type=date]::-webkit-inner-spin-button, input[type=date]::-webkit-outer-spin-button
	{
	-webkit-appearance: none;
	margin: 0;
}

input[type=time]::-webkit-inner-spin-button, input[type=time]::-webkit-outer-spin-button
	{
	-webkit-appearance: none;
	margin: 0;
}
</style>
<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
     	var base_url = "<?php echo base_url();?>";
     	<?php if(ENVIRONMENT == 'production'){?>
     	var parent_url = "http://cineplant.com/nextclick/";
     	<?php }else{?>
     	var parent_url = "http://localhost/nextclick/";
     	<?php }?>
     </script>
</head>
<body>
	<section class="body">

		<!-- start: header -->
            <?php include_once('header.php');?>
            <!-- end: header -->

		<div class="inner-wrapper">
			<!-- start: sidebar -->
               <?php include_once('side_nav.php');?>
                <!-- end: sidebar -->

			<section role="main" class="content-body">
				<header class="page-header">
					<h2><?=ucwords($page_title);?></h2>

					<div class="right-wrapper text-right mr-3">
						<ol class="breadcrumbs">
							<li><a href=""> <i class="fas fa-home"></i>
							</a></li>
							<!--<li><span>Layouts</span></li>
                                <li><span>Sidebar Size SM</span></li>-->
						</ol>


					</div>
				</header>

				<!-- start: page -->
                    
<?php include 'main/'.$page_name.'.php';?>

                    <!-- end: page -->
			</section>
		</div>



	</section>
        
        
       
        

       <?php include_once('scripts.php');?>

    </body>

</html>