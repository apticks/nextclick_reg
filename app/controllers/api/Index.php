<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Index extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('api/index_model');
    }
    public function registeration_post(){
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('post_applying', 'Name', 'trim|required');
        $this->form_validation->set_rules('father_name', 'Father Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]', array(
            'is_unique' => 'This %s already exists.'
        ));
        $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|is_unique[users.mobile]', array(
            'is_unique' => 'This %s already exists.'
        ));
        $this->form_validation->set_rules('whatsapp_number', 'WhatsApp Number', 'required|is_unique[users.whatsapp_number]', array(
            'is_unique' => 'This %s already exists.'
        ));
        $this->form_validation->set_rules('permanent_address', 'Permanent Address', 'trim|required');
        $this->form_validation->set_rules('current_address', 'Current Address', 'trim|required');
        $this->form_validation->set_rules('postal', 'Postal', 'trim|required');
        $this->form_validation->set_rules('state', 'State', 'trim|required');
        $this->form_validation->set_rules('district', 'District', 'trim|required');
        $this->form_validation->set_rules('constituency', 'Constituency', 'trim|required');
        $this->form_validation->set_rules('dob', 'Date Of Birth', 'trim|required');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('marital_status', 'Marital Status', 'trim|required');
        $this->form_validation->set_rules('languages', 'Languages', 'trim|required');
        $this->form_validation->set_rules('e_qualifications', 'Educational Qualification', 'trim|required');
        $this->form_validation->set_rules('job_experience', 'Job Experience', 'trim|required');
        if($this->input->post('job_experience')!='No Experience'){
        $this->form_validation->set_rules('worked_before', 'Worked Before', 'trim|required');
        }
        $this->form_validation->set_rules('id_proof', 'ID Proof', 'trim|required');
        $this->form_validation->set_rules('ssc', 'SSC', 'trim|required');
        $this->form_validation->set_rules('inter', 'Inter Certificate', 'trim|required');
        $this->form_validation->set_rules('degree', 'Degree Certificate', 'trim|required');
         if ($this->form_validation->run() == FALSE) {
            $data = [
                'status' =>  FALSE,
                'message'=> explode('.', validation_errors())[0]
            ];
            $this->set_response($data, REST_Controller:: HTTP_NON_AUTHORITATIVE_INFORMATION);
        } else {
            $input=$this->input->post();
/*        $first_name = $this->input->post('first_name');
        $last_name=$this->input->post('last_name');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $mobile = $this->input->post('mobile');*/
        $userData = array(
                        'post_applying' => $input['post_applying'],
                        'name' => $input['name'],
                        'father_name' => $input['father_name'],
                        'email' => $input['email'],
                        'mobile'=>$input['mobile'],
                        'mobile2'=>$input['mobile2'],
                        'whatsapp_number'=>$input['whatsapp_number'],
                        'permanent_address'=>$input['permanent_address'],
                        'current_address'=>$input['current_address'],
                        'postal'=>$input['postal'],
                        'state'=>$input['state'],
                        'district'=>$input['district'],
                        'constituency'=>$input['constituency'],
                        'dob'=>date('Y-m-d',strtotime($input['dob'])),
                        'gender'=>$input['gender'],
                        'marital_status'=>$input['marital_status'],
                        'languages'=>$input['languages'],
                        'e_qualifications'=>$input['e_qualifications'],
                        'job_experience'=>$input['job_experience'],
                        'worked_before'=>$input['worked_before'],
                        'role_id'=>2
                    );
                    
        $insert_id = $this->crud_model->insert_user_details($userData);

        $user_id=$this->db->insert_id();        
        /*$userfile= $this->post('id_proof');
        $data = $userfile;
        list( $data) = explode(';', $data);
        list($data) = explode(',', $data);
        $data = base64_decode($data);
        $file=file_put_contents("uploads/id_proofs/" . $user_id . '.jpg', $data);*/

        $profile= $this->post('profile');
        list($type, $profile) = explode(';', $profile);
        list(, $profile)      = explode(',', $profile);
        $profile = base64_decode($profile);
        $file=file_put_contents("uploads/profiles/" . $user_id . '.jpg', $profile);

        $id_proof= $this->post('id_proof');
        list($type, $id_proof) = explode(';', $id_proof);
        list(, $id_proof)      = explode(',', $id_proof);
        $id_proof = base64_decode($id_proof);
        $file=file_put_contents("uploads/id_proofs/" . $user_id . '.jpg', $id_proof);

        $ssc= $this->post('ssc');
        $ssc = $ssc;
        list($type, $ssc) = explode(';', $ssc);
        list(, $ssc)      = explode(',', $ssc);
        $ssc = base64_decode($ssc);
        $file=file_put_contents("uploads/ssc/" . $user_id . '.jpg', $ssc);

        $inter= $this->post('inter');
        $inter = $inter;
        list($type, $inter) = explode(';', $inter);
        list(, $inter)      = explode(',', $inter);
        $inter = base64_decode($inter);
        $file=file_put_contents("uploads/inter/" . $user_id . '.jpg', $inter);

        $degree= $this->post('degree');
        $degree = $degree;
        list($type, $degree) = explode(';', $degree);
        list(, $degree)      = explode(',', $degree);
        $degree = base64_decode($degree);
        $file=file_put_contents("uploads/degree/" . $user_id . '.jpg', $degree);

        $data = [
            'status' =>  true,
            'message'=>'ok',
            'data'=> $userData
        ];
        $this->set_response($data, REST_Controller:: HTTP_OK);
        
    }
    }

    public function EmailVerification_post()
    {
        $input=$this->input->post();
        $email=$input['email'];
        $sub=$input['sub'];
        $mes=$input['mes'];
        $this->load->model('email_model');
        $this->email_model->do_email($email,$sub,$mes);
        $data = [
            'status' =>  true,
            'message'=>'Successfully Sended'
        ];
        $this->set_response($data, REST_Controller:: HTTP_OK);
        /*echo parent_site_url();*/
       /* $input=$this->input->post();
        $email=$input['email'];
        $otp=$input['otp'];*/
        /*echo '<script>alert("hi");</script>';*/
        //$this->set_response($data, REST_Controller:: HTTP_OK);
    }

    public function terms_get(){
        $data = [
            'status' =>  true,
            'message'=>'ok',
            
            'data' => $this->db->select('description')->get_where('settings',(['setting_type' => "terms" ]))->row()
       
            
        ];
        $this->set_response($data, REST_Controller:: HTTP_OK);
    }
    public function youtube_get(){
        $data = [
            'status' =>  true,
            'message'=>'ok',
            'data'=> $this->db->select('description')->get_where('settings',['setting_type' => 'youtube'])->row()
        ];
        $this->set_response($data, REST_Controller:: HTTP_OK);
    }
    
    public function profile_pic_post($type = 'r'){
        $_POST = json_decode(file_get_contents("php://input"), TRUE);
        $user_id = $this->db->get_where('users', ['unique_id' => $this->input->post('unique_id')])->row()->id;
        if($type == 'r'){
            $data = [
                'status' => TRUE,
                'message' => 'Success..!'
            ];
            $data['image'] = base_url().'uploads/profiles/'.$user_id.'.jpg';
            $this->set_response($data, REST_Controller:: HTTP_OK);
        }elseif ($type == 'u'){
            file_put_contents("./uploads/profiles/$user_id.jpg", base64_decode($this->input->post('image')));
            $data = [
                'status' => TRUE,
                'message' => 'image updated..!'
            ];
            $this->set_response($data, REST_Controller:: HTTP_OK);
        }
    }
}
