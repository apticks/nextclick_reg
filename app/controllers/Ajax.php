<?php

class Ajax extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
    }
    function get_plans_by_id($id){
    	$plans=$this->crud_model->get_plans_by_id($id);
    foreach ($plans as $row) {
    echo '<label class="btn btn-secondary active"><input type="checkbox" autocomplete="off" name="plans[]" value="'.$row['id'].'">'.ucwords($row['plan']).'</label>';
	}
                                                        
    }
    function get_plan_options_by_id($id){
    	$plans=$this->crud_model->get_plans_by_id($id);
    	echo '<option value="">-- Select Plan --</option>';
    foreach ($plans as $row) {
    echo '<option name="plan" value="'.$row['id'].'">'.ucwords($row['plan']).'</option>';
	}
                                                        
    }
    function get_subjects_by_id($id){
    	$plans=$this->crud_model->get_subjects_by_id($id);
    	echo '<option value="">Select Subject</option>';
    foreach ($plans as $row) {
    echo '<option value="'.$row['id'].'">'.$row['subject'].'</option>';
	}
                                                        
    }
}