<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Main extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('login_id')==''){
            redirect('admin');
        }
        
    }
  public function index()
    {
        $page_data['page_title'] = 'Dashboard';
        $page_data['page_name'] = 'dashboard';
        $this->load->view('backend/index', $page_data);
    }
    
    public function update_profile()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['profile']['name'] !== '') {
            unlink( "./uploads/profiles/$user_id.jpg");
            move_uploaded_file($_FILES['profile']['tmp_name'], "./uploads/profiles/$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    
    public function update_id_proof()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['id_proof']['name'] !== '') {
            unlink("./uploads/id_proofs/$user_id.jpg");
            move_uploaded_file($_FILES['id_proof']['tmp_name'], "./uploads/id_proofs/$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    
    public function update_ssc()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['ssc']['name'] !== '') {
            unlink("./uploads/ssc/$user_id.jpg");
            move_uploaded_file($_FILES['ssc']['tmp_name'], "./uploads/ssc/$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    
    public function update_inter()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['inter']['name'] !== '') {
            unlink("./uploads/inter/$user_id.jpg");
            move_uploaded_file($_FILES['inter']['tmp_name'], "./uploads/inter/$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }
    public function update_degree()
    {
        $user_id = $this->input->post('id');
        if ($_FILES['degree']['name'] !== '') {
            unlink("./uploads/degree/$user_id.jpg");
            move_uploaded_file($_FILES['degree']['tmp_name'], "./uploads/degree/$user_id.jpg");
        }
        redirect('view_user_info/'.$user_id);
    }

    
    public function users(){
        $page_data['mainuser_data']=$this->crud_model->get_users_info();
        $page_data['page_title'] = 'Users';
        $page_data['page_name'] = 'users';
       
        $this->load->view('backend/index', $page_data);
    }
    public function user_delete(){
         echo $this->crud_model->delete_user($this->input->post('id'));
    }
    public function selected_user(){
        $page_data['selecteduser_data']=$this->crud_model->get_selected_users_info();
        $page_data['page_title'] = 'Selected Users';
        $page_data['page_name'] = 'selected_user';
        $this->load->view('backend/index', $page_data);
    }
      public function change_status(){
         echo $this->crud_model->change_status($this->input->post('id'), $this->input->post('is_checked'));
    }
     public function view_user_info($id)
    {
        $page_data['user_id']=$id;
        $page_data['page_title'] = 'View User Info';
        $page_data['page_name'] = 'view_user_info';
        header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
        header("Pragma: no-cache"); // HTTP 1.0.
        header("Expires: 0");
        $this->load->view('backend/index', $page_data);
    }
     public function view_user_info_print($id)
    {
        $page_data['user_id']=$id;
        $page_data['page_title'] = 'View User Info';
        $page_data['page_name'] = 'view_user_info_print';
        $this->load->view('backend/main/view_user_info_print', $page_data);
    }
    public function update_userid(){
        $this->db->where('id', $this->input->post('id'));
        echo $this->db->update('users',['unique_id' => $this->input->post('unique_id')]);
    }
    function system_settings($param1 = '') {
    if($this->input->post()){
            $res=$this->crud_model->update_system_settings();
            if($res){
                $this->session->set_flashdata('success_message', 'Settings Updated');
            }else{
                $this->session->set_flashdata('error_message', 'Settings Not Updated');
            }
            redirect(base_url() . 'settings', 'refresh');
        }
        $page_data['page_name'] = 'settings';
        $page_data['page_title'] = 'system settings';
        $page_data['settings'] = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
        }
        function change_password() {
      if($this->input->post()){
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
        if ($this->form_validation->run() == TRUE) {
        $user_id=$this->session->userdata('login_id');
        $password=$this->input->post('password');
            $res=$this->crud_model->update_user_password($password,$user_id);
            if($res){
                $this->session->set_flashdata('success_message', 'Password Updated');
            }else{
                $this->session->set_flashdata('error_message', 'Password Not Updated');
            }
            redirect(base_url() . 'change_password', 'refresh');
        }
    }
        $page_data['page_name'] = 'change_password';
        $page_data['page_title'] = 'Change Password';
        $this->load->view('backend/index', $page_data);
        }
        
}
?>